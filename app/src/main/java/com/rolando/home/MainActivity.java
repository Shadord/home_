package com.rolando.home;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.rolando.home.tools.AppDBHelper;
import com.rolando.home.tools.device.DeviceContract;
import com.rolando.home.tools.room.RoomContract;
import com.rolando.home.ui.room.AddRoomActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    public Context mContext;

    private AppBarConfiguration mAppBarConfiguration;
    private FloatingActionButton routineMorningFab, routineEveningFab, routineInFab, routineOutFab, routineOpenFab;
    private NavController navController;
    NavigationView navigationView;
    private DrawerLayout drawer;

    private SQLiteDatabase mDatabase;
    private SharedPreferences sharedPreferences = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = this;
        initViews();
        initNavigation();
        initFab();

        sharedPreferences = getSharedPreferences("fr.rolando.home", MODE_PRIVATE);
        AppDBHelper dbHelper = new AppDBHelper(this);
        mDatabase = dbHelper.getWritableDatabase();



    }


    public void initViews() {
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        routineMorningFab = findViewById(R.id.routine_morning_fab);
        routineEveningFab = findViewById(R.id.routine_evening_fab);
        routineInFab = findViewById(R.id.routine_in_fab);
        routineOutFab = findViewById(R.id.routine_out_fab);
        routineOpenFab = findViewById(R.id.routine_open);
    }

    public void initNavigation() {

        /*View hView = navigationView.getHeaderView(0);
        TextView t = hView.findViewById(R.id.navigation_header_title);
        t.setText("Hello !");*/
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_room ,R.id.nav_settings)
                .setDrawerLayout(drawer)
                .build();


        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                switch (destination.getId()) {
                    case R.id.nav_room : {
                        break;
                    }
                    case R.id.nav_settings : {
                        drawer.setBackgroundColor(Color.WHITE);
                        break;
                    }
                    default: {
                        drawer.setBackground(getResources().getDrawable(R.drawable.background_gradient));
                        break;
                    }
                }
            }
        });
    }

    public void initFab() {
        routineOpenFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFabVisible();
            }
        });
    }

    public void setFabVisible() {
        Animation a = AnimationUtils.loadAnimation(mContext, R.anim.open_routine_animator_in);
        Animation b = AnimationUtils.loadAnimation(mContext, R.anim.open_routine_animator_out);
        b.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                routineOpenFab.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        routineMorningFab.startAnimation(a);
        routineEveningFab.startAnimation(a);
        routineInFab.startAnimation(a);
        routineOutFab.startAnimation(a);
        routineOpenFab.startAnimation(b);
        routineMorningFab.setVisibility(View.VISIBLE);
        routineEveningFab.setVisibility(View.VISIBLE);
        routineInFab.setVisibility(View.VISIBLE);
        routineOutFab.setVisibility(View.VISIBLE);
    }

    public void setFabInvisible() {
        Animation a = AnimationUtils.loadAnimation(mContext, R.anim.open_routine_animator_out);
        Animation b = AnimationUtils.loadAnimation(mContext, R.anim.open_routine_animator_in);
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                routineMorningFab.setVisibility(View.GONE);
                routineEveningFab.setVisibility(View.GONE);
                routineInFab.setVisibility(View.GONE);
                routineOutFab.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        routineMorningFab.startAnimation(a);
        routineEveningFab.startAnimation(a);
        routineInFab.startAnimation(a);
        routineOutFab.startAnimation(a);
        routineOpenFab.startAnimation(b);
        routineOpenFab.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_room_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.add_room) {
            Intent i = new Intent(mContext, AddRoomActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(ev.getAction() == MotionEvent.ACTION_DOWN) {
            if(routineOpenFab.getVisibility() == View.GONE) {
                setFabInvisible();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPreferences.getBoolean("FirstRun", true)) {
            insertRooms();
            insertDevices();
            Toast.makeText(mContext, "First Run", Toast.LENGTH_SHORT).show();

            sharedPreferences.edit().putBoolean("FirstRun", false).commit();
        }
    }

    public void insertRooms() {
        mDatabase.delete(RoomContract.RoomEntry.TABLE_NAME, null, null);
        ContentValues cv = new ContentValues();
        cv.put(RoomContract.RoomEntry.COLUMN_NAME, "Living Room");
        cv.put(RoomContract.RoomEntry.COLUMN_TYPE, RoomContract.RoomType.ROOM_TYPE_LIVING_ROOM);
        mDatabase.insert(RoomContract.RoomEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(RoomContract.RoomEntry.COLUMN_NAME, "Bathroom");
        cv.put(RoomContract.RoomEntry.COLUMN_TYPE, RoomContract.RoomType.ROOM_TYPE_BATHROOM);
        mDatabase.insert(RoomContract.RoomEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(RoomContract.RoomEntry.COLUMN_NAME, "BedRoom");
        cv.put(RoomContract.RoomEntry.COLUMN_TYPE, RoomContract.RoomType.ROOM_TYPE_BEDROOM);
        mDatabase.insert(RoomContract.RoomEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(RoomContract.RoomEntry.COLUMN_NAME, "DeskRoom");
        cv.put(RoomContract.RoomEntry.COLUMN_TYPE, RoomContract.RoomType.ROOM_TYPE_DESKROOM);
        mDatabase.insert(RoomContract.RoomEntry.TABLE_NAME, null, cv);
    }

    public void insertDevices() {
        mDatabase.delete(DeviceContract.DeviceEntry.TABLE_NAME, null, null);
        ContentValues cv = new ContentValues();
        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "Plafonnier");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_LIGHT);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 1);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-26 11:52:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);

        cv.clear();
        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "Lampe TV");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_RGBLIGHT);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 1);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-26 11:52:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "TV");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_TV);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 1);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-26 11:30:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "Lampe de chevet");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_DIMMABLELIGHT);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 2);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-26 11:40:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "Clim");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_CLIM);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 2);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-23 19:20:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "Chauffage");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_HEATER);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 2);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-26 11:40:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "Chaudière");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_BOILER);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 2);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-26 11:40:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "Enceinte");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_SPEAKER);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 2);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-26 11:40:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);
        cv.clear();

        cv.put(DeviceContract.DeviceEntry.COLUMN_NAME, "Plante");
        cv.put(DeviceContract.DeviceEntry.COLUMN_TYPE, DeviceContract.DeviceType.DEVICE_TYPE_ARDUINO);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ADDRESS, "192.168.1.35");
        cv.put(DeviceContract.DeviceEntry.COLUMN_ROOM_ID, 2);
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, false);
        cv.put(DeviceContract.DeviceEntry.COLUMN_LAST_USE, "2020-10-26 11:40:00");
        mDatabase.insert(DeviceContract.DeviceEntry.TABLE_NAME, null, cv);
    }

    public void openRoom(int room_id) {
        Bundle b = new Bundle();
        b.putInt(RoomContract.RoomEntry.TABLE_NAME, room_id);

        Log.d("HOME_", "HomeFragment : openRoom : room_id = " + b.toString());
        navController.navigate(R.id.nav_room, b);
    }
}