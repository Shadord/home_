package com.rolando.home.ui.room;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rolando.home.R;
import com.rolando.home.tools.AppDBHelper;
import com.rolando.home.tools.device.DeviceAdapter;
import com.rolando.home.tools.device.DeviceContract;
import com.rolando.home.tools.room.RoomAdapter;
import com.rolando.home.tools.room.RoomContract;


public class RoomFragment extends Fragment {


    private DeviceAdapter mDeviceAdapter;
    private SQLiteDatabase mDatabase;
    private int room_id = -1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Bundle b = getArguments();
        if(b != null) {

            int room_id = b.getInt(RoomContract.RoomEntry.TABLE_NAME, -1);
            if(room_id > 0) {
                this.room_id = room_id;
            }else{
                //ERREUR
                Log.d("HOME_", "RoomFragment : ERROR_BUNDLE : room_id = " + room_id);
            }
        }else{
            //ERREUR
            Log.d("HOME_", "RoomFragment : ERROR_BUNDLE : savedInstanceBuncle = null");
        }
        View root = inflater.inflate(R.layout.fragment_room, container, false);
        AppDBHelper dbHelper = new AppDBHelper(getContext());
        mDatabase = dbHelper.getWritableDatabase();
        initRecyclerView(root);
        return root;
    }

    public void initRecyclerView(View v) {
        Log.d("HOME_", "RoomFragment : INITIALIZE ADAPTER");
        RecyclerView deviceRecyclerView = v.findViewById(R.id.fragment_room_recycler_view);
        deviceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mDeviceAdapter = new DeviceAdapter(getContext(), getAllDevicesItem(), mDatabase);
        deviceRecyclerView.setAdapter(mDeviceAdapter);
    }

    public Cursor getAllDevicesItem() {
        Log.d("HOME_", "RoomFragment : INITIALIZE ITEMS");
        String[] s = {room_id+""};
        Cursor c =mDatabase.query(
                DeviceContract.DeviceEntry.TABLE_NAME,
                null,
                DeviceContract.DeviceEntry.COLUMN_ROOM_ID + "=?",
                s,
                DeviceContract.DeviceEntry.COLUMN_TYPE,
                null,
                null
        );
        Log.d("HOME_", "RoomFragment : INITIALIZE ITEMS : "+ c.toString());


        return  c;
    }



}