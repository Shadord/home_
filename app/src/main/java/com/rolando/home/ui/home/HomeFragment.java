package com.rolando.home.ui.home;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rolando.home.R;
import com.rolando.home.tools.AppDBHelper;
import com.rolando.home.tools.room.RoomContract;
import com.rolando.home.tools.room.RoomAdapter;

public class HomeFragment extends Fragment {

    private RoomAdapter mRoomAdapter;
    private SQLiteDatabase mDatabase;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        AppDBHelper dbHelper = new AppDBHelper(getContext());
        mDatabase = dbHelper.getWritableDatabase();
        initRecyclerView(root);
        return root;
    }

    public void initRecyclerView(View v) {
        Log.d("HOME_", "HomeFragment : INITIALIZE ADAPTER");
        RecyclerView roomRecyclerView = v.findViewById(R.id.fragment_home_recycler_view);
        roomRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRoomAdapter = new RoomAdapter(getContext(), getAllRoomsItem());
        roomRecyclerView.setAdapter(mRoomAdapter);
    }

    public Cursor getAllRoomsItem() {
        Log.d("HOME_", "HomeFragment : INITIALIZE ITEMS");
        return mDatabase.query(
                RoomContract.RoomEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }
}