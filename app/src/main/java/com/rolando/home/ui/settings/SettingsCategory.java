package com.rolando.home.ui.settings;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceViewHolder;

import com.rolando.home.R;


public class SettingsCategory extends PreferenceCategory {

    public SettingsCategory(Context context) {
        super(context);
    }

    public SettingsCategory(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SettingsCategory(Context context, AttributeSet attrs,
                                int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);

        TextView titleView = (TextView) holder.findViewById(android.R.id.title);
        titleView.setTextColor(Color.rgb(00,150,200));


    }
}