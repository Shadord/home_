package com.rolando.home.tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.rolando.home.tools.device.DeviceContract;
import com.rolando.home.tools.room.RoomContract;

public class AppDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "homePlus.db";
    public static final int DATABASE_VERSION = 1;

    public AppDBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_ROOM_TABLE = "CREATE TABLE " +
                RoomContract.RoomEntry.TABLE_NAME + " (" +
                RoomContract.RoomEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                RoomContract.RoomEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                RoomContract.RoomEntry.COLUMN_TYPE + " TEXT NOT NULL " +
                ");";


        final String SQL_CREATE_DEVICES_TABLE = "CREATE TABLE " +
                DeviceContract.DeviceEntry.TABLE_NAME + " (" +
                DeviceContract.DeviceEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DeviceContract.DeviceEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                DeviceContract.DeviceEntry.COLUMN_ADDRESS + " TEXT NOT NULL, " +
                DeviceContract.DeviceEntry.COLUMN_ROOM_ID + " INTEGER NOT NULL, " +
                DeviceContract.DeviceEntry.COLUMN_TYPE + " TEXT NOT NULL, " +
                DeviceContract.DeviceEntry.COLUMN_ACTIVE + " BOOLEAN NOT NULL, " +
                DeviceContract.DeviceEntry.COLUMN_LAST_USE + " DATETIME NOT NULL " +
                ");";

        sqLiteDatabase.execSQL(SQL_CREATE_ROOM_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_DEVICES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RoomContract.RoomEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DeviceContract.DeviceEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
