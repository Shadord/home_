package com.rolando.home.tools.room;

import android.provider.BaseColumns;

public class RoomContract  {

    public RoomContract() {
    }
    public class RoomEntry implements BaseColumns {
        public static final String TABLE_NAME = "room_table";
        public static final String COLUMN_NAME = "room_name";
        public static final String COLUMN_TYPE = "room_type";
    }

    public class RoomType {
        public static final String ROOM_TYPE_LIVING_ROOM = "LIVING_ROOM";
        public static final String ROOM_TYPE_BATHROOM = "BATHROOM";
        public static final String ROOM_TYPE_BEDROOM = "BEDROOM";
        public static final String ROOM_TYPE_DESKROOM = "DESKROOM";
        public static final String ROOM_TYPE_KITCHEN = "KITCHEN";
        public static final String ROOM_TYPE_GARAGE = "GARAGE";
        public static final String ROOM_TYPE_GAME_ROOM = "GAME_ROOM";
        public static final String ROOM_TYPE_UTILITY_ROOM = "UTILITY_ROOM";
        public static final String ROOM_TYPE_MUD_ROOM = "MUD_ROOM";
    }

}
