package com.rolando.home.tools.device;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.github.angads25.toggle.LabeledSwitch;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.rolando.home.R;
import com.rolando.home.tools.AppDBHelper;
import com.rolando.home.tools.device.DeviceContract;
import com.rolando.home.tools.utilities.ImageSelector;
import com.rolando.home.ui.room.RoomFragment;

import java.util.Random;


public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.DeviceViewHolder>{

    private Cursor mCursor;
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private static OnUpdateListener mOnUpdateListener;

    public DeviceAdapter(Context context, Cursor cursor, SQLiteDatabase database) {
        mContext = context;
        mCursor = cursor;
        mDatabase = database;
    }

    public interface OnUpdateListener {
        void onUpdate(View view, int pos);
    }

    public void setOnUpdateListener(OnUpdateListener listener) {
        mOnUpdateListener = listener;
    }


    @NonNull
    @Override
    public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.device_adapter, parent, false);
        return new DeviceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DeviceViewHolder holder, int position) {
        final int pos = position;
        String old_type;
        if(!mCursor.moveToPosition(position)) {
            return;
        }
        if(mCursor.isFirst())
            old_type = "ZZ";
        else {
            mCursor.moveToPrevious();
            old_type = mCursor.getString(mCursor.getColumnIndex(DeviceContract.DeviceEntry.COLUMN_TYPE));
            mCursor.moveToNext();
        }

        String type = mCursor.getString(mCursor.getColumnIndex(DeviceContract.DeviceEntry.COLUMN_TYPE));
        if(old_type.substring(0, 2).equals(type.substring(0, 2))) {
            holder.deviceType.setVisibility(View.GONE);
        }else{
            switch (type) {
                case DeviceContract.DeviceType.DEVICE_TYPE_LIGHT :
                case DeviceContract.DeviceType.DEVICE_TYPE_RGBLIGHT :
                case DeviceContract.DeviceType.DEVICE_TYPE_DIMMABLELIGHT : {
                    type = DeviceContract.DeviceType.DEVICE_TYPE_LIGHT;
                }

            }
            holder.deviceType.setText(type.toUpperCase());
        }
        
        final String id = mCursor.getString(mCursor.getColumnIndex(DeviceContract.DeviceEntry._ID));

        final String name = mCursor.getString(mCursor.getColumnIndex(DeviceContract.DeviceEntry.COLUMN_NAME));
        holder.deviceName.setText(name);



        if(mCursor.getInt(mCursor.getColumnIndex(DeviceContract.DeviceEntry.COLUMN_ACTIVE)) > 0) {
            holder.deviceActivation.setChecked(true);
            deviceController(holder, true);
        }else{
            holder.deviceActivation.setChecked(false);
            deviceController(holder, false);
        }

        holder.deviceActivation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(mContext, name+" switched " + b, Toast.LENGTH_SHORT).show();
                updateItem(b, pos);
                refreshCursor();
                notifyDataSetChanged();
            }
        });



        holder.deviceConso.setText("10 W");

        holder.deviceImage.setImageDrawable(ImageSelector.DrawableDeviceSelector(mContext, mCursor));

        Log.d("HOME+", "DeviceAdapter : " + name);


    }

    public void deviceController(DeviceViewHolder holder, boolean visibility) {
        holder.deviceSeekBarControl.setVisibility(View.GONE);
        holder.deviceUpDownControl.setVisibility(View.GONE);
        holder.deviceFanControl.setVisibility(View.GONE);
        switch(mCursor.getString(mCursor.getColumnIndex(DeviceContract.DeviceEntry.COLUMN_TYPE))) {
            case DeviceContract.DeviceType.DEVICE_TYPE_CLIM : {
                holder.deviceFanControl.setVisibility(visibility ? View.VISIBLE : View.GONE);
                break;
            }
            case DeviceContract.DeviceType.DEVICE_TYPE_SPEAKER : {
                holder.deviceSeekBarControl.setVisibility(visibility ? View.VISIBLE : View.GONE);
                holder.deviceSeekBarControlImage.setImageResource(R.drawable.device_sound);
                break;
            }
            case DeviceContract.DeviceType.DEVICE_TYPE_DIMMABLELIGHT:
            case DeviceContract.DeviceType.DEVICE_TYPE_RGBLIGHT : {
                holder.deviceSeekBarControl.setVisibility(visibility ? View.VISIBLE : View.GONE);
                holder.deviceSeekBarControlImage.setImageResource(R.drawable.device_light);
                break;
            }
            default : {
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    public void swapCursor(Cursor newCursor) {
        if(mCursor != null) {
            mCursor.close();
        }
        mCursor = newCursor;
        if(newCursor != null) {
            notifyDataSetChanged();
        }
    }

    public void refreshCursor() {
        RoomFragment roomFragment = (RoomFragment) mContext;

        mCursor =
    }

    public void updateItem(boolean b, int pos) {
        ContentValues cv = new ContentValues();
        cv.put(DeviceContract.DeviceEntry.COLUMN_ACTIVE, b);
        String[] strings = {mCursor.getInt(mCursor.getColumnIndex(DeviceContract.DeviceEntry._ID)) + ""};
        mDatabase.update(DeviceContract.DeviceEntry.TABLE_NAME,
                cv,
                DeviceContract.DeviceEntry._ID + "=?",
                strings);
    }


    public class DeviceViewHolder extends RecyclerView.ViewHolder {

        private View mView;
        private TextView deviceType;
        private TextView deviceName, deviceConso;
        private ImageView deviceImage;
        private Switch deviceActivation;
        private RelativeLayout deviceUpDownControl, deviceFanControl, deviceSeekBarControl;
        private ImageView deviceUpDownControlUp, deviceUpDownControlDown, deviceUpDownControlStop;
        private ImageView deviceFanControlUp, deviceFanControlDown, deviceUpDownControlFan;
        private SeekBar deviceSeekBarControlSeekBar;
        private ImageView deviceSeekBarControlImage;

        public DeviceViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            deviceType = itemView.findViewById(R.id.device_type);
            deviceName = itemView.findViewById(R.id.device_name);
            deviceImage = itemView.findViewById(R.id.device_image);
            deviceConso = itemView.findViewById(R.id.device_conso);
            deviceActivation = itemView.findViewById(R.id.device_activation);

            deviceUpDownControl = itemView.findViewById(R.id.device_updown_control);
            deviceFanControl = itemView.findViewById(R.id.device_fan_control);
            deviceSeekBarControl = itemView.findViewById(R.id.device_seekbar_control);

            deviceUpDownControlUp = itemView.findViewById(R.id.device_updown_control_up);
            deviceUpDownControlDown = itemView.findViewById(R.id.device_updown_control_down);
            deviceUpDownControlStop = itemView.findViewById(R.id.device_updown_control_stop);
            deviceFanControlUp = itemView.findViewById(R.id.device_fan_control_up);
            deviceFanControlDown = itemView.findViewById(R.id.device_fan_control_down);
            deviceUpDownControlFan = itemView.findViewById(R.id.device_fan_control_fan);
            deviceSeekBarControlSeekBar = itemView.findViewById(R.id.device_seekbar_control_seekbar);
            deviceSeekBarControlImage = itemView.findViewById(R.id.device_seekbar_control_image);
            Log.d("HOME+", "DeviceAdapter : INITIALIZE VIEWHOLDER");

        }

    }

}
