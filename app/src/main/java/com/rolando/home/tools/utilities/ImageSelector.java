package com.rolando.home.tools.utilities;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;

import androidx.core.content.res.ResourcesCompat;

import com.rolando.home.R;
import com.rolando.home.tools.device.DeviceContract;
import com.rolando.home.tools.room.RoomContract;


public final class ImageSelector {
    public ImageSelector() {
    }

    public static Drawable DrawableRoomSelector(Context mContext, Cursor cursor) {
        Drawable d = null;
        switch (cursor.getString(cursor.getColumnIndex(RoomContract.RoomEntry.COLUMN_TYPE))) {
            case RoomContract.RoomType.ROOM_TYPE_BATHROOM : {
                //d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.room_bathroom, null);
                break;
            }
            case RoomContract.RoomType.ROOM_TYPE_BEDROOM : {
                //d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.room_bedroom, null);
                break;
            }
            case RoomContract.RoomType.ROOM_TYPE_DESKROOM : {
                //d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.room_deskroom, null);
                break;
            }
            case RoomContract.RoomType.ROOM_TYPE_KITCHEN : {
                //d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.room_kitchen, null);
                break;
            }
            case RoomContract.RoomType.ROOM_TYPE_LIVING_ROOM : {
                //d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.room_livingroom, null);
                break;
            }
            case RoomContract.RoomType.ROOM_TYPE_MUD_ROOM : {
                //d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.room_mudroom, null);
                break;
            }
            default : {
                //d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.room_default, null);
                break;
            }
        }
        return d;
    }

    public static Drawable DrawableDeviceSelector(Context mContext, Cursor cursor) {
        Drawable d = null;
        switch (cursor.getString(cursor.getColumnIndex(DeviceContract.DeviceEntry.COLUMN_TYPE))) {
            case DeviceContract.DeviceType.DEVICE_TYPE_CLIM : {
                d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.device_air_conditionner, null);
                break;
            }
            case DeviceContract.DeviceType.DEVICE_TYPE_RGBLIGHT :
            case DeviceContract.DeviceType.DEVICE_TYPE_DIMMABLELIGHT :
            case DeviceContract.DeviceType.DEVICE_TYPE_LIGHT : {
                d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.device_light, null);
                break;
            }
            case DeviceContract.DeviceType.DEVICE_TYPE_SPEAKER: {
                d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.device_speaker, null);
                break;
            }
            case DeviceContract.DeviceType.DEVICE_TYPE_TV: {
                d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.device_tv, null);
                break;
            }
            case DeviceContract.DeviceType.DEVICE_TYPE_HEATER: {
                d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.device_heater, null);
                break;
            }
            case DeviceContract.DeviceType.DEVICE_TYPE_BOILER: {
                d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.device_boiler, null);
                break;
            }
            default : {
                d = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.ic_add, null);
                break;
            }
        }
        return d;
    }
}
