package com.rolando.home.tools.room;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rolando.home.MainActivity;
import com.rolando.home.R;


public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.RoomViewHolder> {

    private Cursor mCursor;
    private Context mContext;

    public RoomAdapter(Context context, Cursor cursor) {
        mContext = context;
        mCursor = cursor;
    }


    @NonNull
    @Override
    public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.room_adapter, parent, false);
        return new RoomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomViewHolder holder, int position) {
        final int pos = position;
        if(!mCursor.moveToPosition(position)) {

            return;
        }

        final String id = mCursor.getString(mCursor.getColumnIndex(RoomContract.RoomEntry._ID));

        final String name = mCursor.getString(mCursor.getColumnIndex(RoomContract.RoomEntry.COLUMN_NAME));
        holder.roomName.setText(name);

        Log.d("HOME+", "RoomAdapter : " + name);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // On ouvre la pièce
                MainActivity A = (MainActivity) mContext;
                A.openRoom(Integer.parseInt(id));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    public void swapCursor(Cursor newCursor) {
        if(mCursor != null) {
            mCursor.close();
        }
        mCursor = newCursor;
        if(newCursor != null) {
            notifyDataSetChanged();
        }
    }

    public class RoomViewHolder extends RecyclerView.ViewHolder {

        private View mView;
        private TextView roomName, roomTemp;
        private ImageView roomImage, roomLightIndicator;

        public RoomViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            roomName = itemView.findViewById(R.id.room_name);
            roomImage = itemView.findViewById(R.id.room_image);
            roomTemp = itemView.findViewById(R.id.room_temp);
            roomLightIndicator = itemView.findViewById(R.id.room_light_indicator);
            Log.d("HOME+", "RoomAdapter : INITIALIZE VIEWHOLDER");

        }
    }

}
