package com.rolando.home.tools.device;

import android.provider.BaseColumns;

public class DeviceContract {

    public DeviceContract() {
    }
    public class DeviceEntry implements BaseColumns {
        public static final String TABLE_NAME = "device_table";
        public static final String COLUMN_NAME = "device_name";
        public static final String COLUMN_ADDRESS = "device_address";
        public static final String COLUMN_TYPE = "device_type";
        public static final String COLUMN_ROOM_ID = "device_room_id";
        public static final String COLUMN_LAST_USE = "device_last_use";
        public static final String COLUMN_ACTIVE = "device_active";
    }

    public class DeviceType {
        public static final String DEVICE_TYPE_LIGHT = "LIGHT";
        public static final String DEVICE_TYPE_DIMMABLELIGHT = "LIGHT_DIMMABLE";
        public static final String DEVICE_TYPE_RGBLIGHT = "LIGHT_RGB";
        public static final String DEVICE_TYPE_HEATER = "HEATER";
        public static final String DEVICE_TYPE_BOILER = "BOILER";
        public static final String DEVICE_TYPE_CLIM = "CLIM";
        public static final String DEVICE_TYPE_SPEAKER = "SPEAKER";
        public static final String DEVICE_TYPE_TV = "TV";
        public static final String DEVICE_TYPE_ARDUINO = "ARDUINO";
    }

}
